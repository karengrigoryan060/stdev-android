package managerworkhelper.stdevapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class LoginPageActivity extends AppCompatActivity {

    private EditText login_et;
    private EditText password_et;
    private TextView error_massage_tv;
    private LinearLayout progress_bar_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        findFilds();

    }

    private void findFilds() {
        login_et = (EditText) findViewById(R.id.login_et);
        password_et = (EditText) findViewById(R.id.password_et);
        error_massage_tv=(TextView)findViewById(R.id.error_massage_tv);
        progress_bar_layout=(LinearLayout)findViewById(R.id.progress_bar_layout);
    }

    public void login_btn(View view) {
        String login = login_et.getText().toString();
        String passwprd = password_et.getText().toString();
        if (true) {//isLoginCorrect(login)
            isCorrectLoginAndPassword(login, passwprd);

        } else {
            login_et.setError(getResources().getString(R.string.wrong_email));
        }
    }

    private boolean isCorrectLoginAndPassword(String login, String password) {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://192.168.2.210:8080/login?login="+login+"&password="+password;

        StringRequest stringRequest;
        stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("")){
                            error_massage_tv.setText(getResources().getString(R.string.wrong_login_or_password));
                        }else{
                            Intent i = new Intent(LoginPageActivity.this,ProductPageActivity.class);
                            startActivity(i);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error_massage_tv.setText(getResources().getString(R.string.connection_fail));
            }
        });
        queue.add(stringRequest);

        return false;
    }

    private boolean isLoginCorrect(String login) {
        boolean isCorrect = true;

        //There can be meny cases for verification
        if (login.equals("")) isCorrect = false;
        if (!login.contains("@"))isCorrect = false;
        if (!login.contains("."))isCorrect = false;


        return isCorrect;
    }
}
